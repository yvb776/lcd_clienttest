﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.Net.Sockets;

namespace LCD_ClientTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            try
            {
                TcpClient tcp = new TcpClient("127.0.0.1", 25914);

                NetworkStream ns = tcp.GetStream();

                byte[] send_byte = System.Text.Encoding.UTF8.GetBytes(txt.Text);

                ns.Write(send_byte, 0, send_byte.Length);

                info.Text = "送信成功";

                tcp.Close();
            }
            catch (Exception ex)
            {
                info.Text = ex.Message;
            }
        }

        private void txt_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnSend_Click(sender, e);
            }
        }
    }
}
